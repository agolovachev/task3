package ru.alfabattle.service;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.apache.commons.math3.stat.descriptive.rank.Median;
import org.apache.lucene.util.SloppyMath;
import org.springframework.stereotype.Component;
import ru.alfabattle.mapper.BranchMapper;
import ru.alfabattle.model.Branch;
import ru.alfabattle.model.QueueLog;
import ru.alfabattle.repo.BranchRepository;
import ru.alfabattle.repo.QueueLogRepository;
import ru.alfabattle.rest.ClosestBranchResponseRo;
import ru.alfabattle.rest.PredictedLoadResponseRo;

import java.math.BigDecimal;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class BranchService implements IBranchService {

    @NonNull
    private final BranchRepository branchRepository;

    @NonNull
    private final QueueLogRepository queueLogRepository;

    @NonNull
    private final BranchMapper mapper;

    @Override
    public Optional<Branch> find(Integer id) {
        return branchRepository.findById(id);
    }

    @Override
    public ClosestBranchResponseRo findClosest(BigDecimal lat, BigDecimal lon) {

        Iterable<Branch> branches = branchRepository.findAll();

        BigDecimal minDist = BigDecimal.valueOf(Double.MAX_VALUE);

        Branch result = null;

        for (Branch branch : branches) {
            BigDecimal dist = BigDecimal.valueOf(
                    SloppyMath.haversinMeters(
                            lat.doubleValue(),
                            lon.doubleValue(),
                            branch.getLat().doubleValue(),
                            branch.getLon().doubleValue()));

            if (minDist.compareTo(dist) > 0) {
                minDist = dist;
                result = branch;
            }
        }

        return mapper.transform(result, minDist);
    }

    @Override
    public PredictedLoadResponseRo predict(Integer id, Integer hourOfDay, Integer dayOfWeek) {

        List<QueueLog> queueLogs = queueLogRepository.findByBranchesId(id);

        List<QueueLog> data = queueLogs
                .stream()
                .filter(queueLog -> queueLog.getData().getDayOfWeek().getValue() == dayOfWeek)
                .filter(queueLog -> queueLog.getEndTimeOfWait().getHour() == hourOfDay)
                .collect(Collectors.toList());

        if (data.isEmpty()) {
            return null;
        }

        List<Long> intervals = data.stream()
                .map(dataItem -> ChronoUnit.SECONDS.between(dataItem.getStartTimeOfWait(), dataItem.getEndTimeOfWait()))
                .collect(Collectors.toList());

        int size = (int) intervals.stream().filter(interval -> interval != null).count();
        int i = 0;

        double[] primitiveArray = new double[size];
        for (Long elem : intervals) {
            if (elem == null) {
                continue;
            }
            primitiveArray[i] = Double.valueOf(elem);
            i++;
        }

        double median = (new Median().evaluate(primitiveArray));

        return mapper.transform(branchRepository.findById(id).get(), dayOfWeek, hourOfDay, Math.round(median));
    }


}

package ru.alfabattle.service;

import ru.alfabattle.model.Branch;
import ru.alfabattle.rest.ClosestBranchResponseRo;
import ru.alfabattle.rest.PredictedLoadResponseRo;

import java.math.BigDecimal;
import java.util.Optional;

public interface IBranchService {

    Optional<Branch> find(Integer id);

    ClosestBranchResponseRo findClosest(BigDecimal lat, BigDecimal lon);

    PredictedLoadResponseRo predict(Integer id, Integer hourOfDay, Integer dayOfWeek);
}

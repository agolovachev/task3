package ru.alfabattle.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import ru.alfabattle.model.Branch;
import ru.alfabattle.rest.BranchRo;
import ru.alfabattle.rest.ClosestBranchResponseRo;
import ru.alfabattle.rest.PredictedLoadResponseRo;

import java.math.BigDecimal;

@Mapper
public interface BranchMapper {

    BranchRo transform(Branch branch);

    @Mappings({
            @Mapping(target = "distance", expression = "java(distance != null ? java.lang.Math.round(distance.doubleValue()) : null)"),
    })
    ClosestBranchResponseRo transform(Branch branch, BigDecimal distance);

    PredictedLoadResponseRo transform(Branch branch, Integer dayOfWeek, Integer hourOfDay, Long predicting);
}
package ru.alfabattle.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.time.LocalDate;
import java.time.LocalTime;

@Data
@Entity
@ToString
@EqualsAndHashCode
public class QueueLog {


    @Id
    private Integer id;

    private LocalDate data;
    private LocalTime startTimeOfWait;
    private LocalTime endTimeOfWait;
    private LocalTime endTimeOfService;
    private Integer branchesId;
}

package ru.alfabattle.model;


import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;

@Data
@Entity
@Table(name = "branches")
@ToString
@EqualsAndHashCode
public class Branch {

    @Id
    private Integer id;

    private String address;
    private BigDecimal lat;
    private BigDecimal lon;
    private String title;
}

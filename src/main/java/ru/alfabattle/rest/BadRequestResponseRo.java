package ru.alfabattle.rest;

import lombok.Data;

@Data
public class BadRequestResponseRo implements RestObject {

    private static final long serialVersionUID = -1376622559353789737L;

    private String status;
}

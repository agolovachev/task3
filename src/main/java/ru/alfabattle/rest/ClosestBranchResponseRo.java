package ru.alfabattle.rest;

import lombok.Data;

@Data
public class ClosestBranchResponseRo extends BranchRo {

    private static final long serialVersionUID = 7328528758341130800L;

    private Long distance;
}

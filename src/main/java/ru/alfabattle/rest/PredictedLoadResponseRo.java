package ru.alfabattle.rest;

import lombok.Data;

@Data
public class PredictedLoadResponseRo extends BranchRo {

    private static final long serialVersionUID = -9132176304835394218L;

    private Integer dayOfWeek;
    private Integer hourOfDay;
    private Long predicting;
}

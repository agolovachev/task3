package ru.alfabattle.rest;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class BranchRo implements RestObject {

    private static final long serialVersionUID = 180765242692711791L;

    private Integer id;
    private String address;
    private BigDecimal lat;
    private BigDecimal lon;
    private String title;
}

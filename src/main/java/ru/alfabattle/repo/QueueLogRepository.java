package ru.alfabattle.repo;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import ru.alfabattle.model.QueueLog;

import java.util.List;

@Repository
public interface QueueLogRepository extends PagingAndSortingRepository<QueueLog, Integer> {

    List<QueueLog> findByBranchesId(Integer branchesId);
}

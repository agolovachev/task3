package ru.alfabattle.repo;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import ru.alfabattle.model.Branch;

@Repository
public interface BranchRepository extends PagingAndSortingRepository<Branch, Integer> {

}

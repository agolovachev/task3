package ru.alfabattle.exception;

import com.fasterxml.jackson.databind.JsonMappingException;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import ru.alfabattle.rest.BadRequestResponseRo;

import java.nio.file.AccessDeniedException;

@Slf4j
@ControllerAdvice
@AllArgsConstructor
public class ControllerExceptionHandler {

    @ExceptionHandler(value = {BadRequestDataException.class})
    public ResponseEntity handleBadRequestDataException(BadRequestDataException ex,
                                                        WebRequest request) {
        BadRequestResponseRo responseRo = new BadRequestResponseRo();
        responseRo.setStatus(ex.getMessage());

        return new ResponseEntity(responseRo, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(value = {NotFoundException.class})
    public ResponseEntity handleBadRequestDataException(NotFoundException ex,
                                                        WebRequest request) {
        return new ResponseEntity(HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(value = {Throwable.class})
    public ResponseEntity handleUnexpected(Throwable ex, WebRequest request) {
        log.error("Unknown error for request: "
                + ((ServletWebRequest) request).getRequest().getRequestURI()
                + " from ip " + request.getHeader("X-Real-IP"), ex);
        return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(JsonMappingException.class)
    public ResponseEntity handleBadRequestJsonMappingException(JsonMappingException exception) {
        log.warn("Json mapping exception - {}", exception.getMessage());
        return new ResponseEntity(HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(MethodArgumentTypeMismatchException.class)
    public ResponseEntity handleBadRequestMethodArgumentTypeMismatchException(MethodArgumentTypeMismatchException exception) {
        log.warn("Method argument type mismatch exception - {}", exception.getMessage());

        BadRequestResponseRo responseRo = new BadRequestResponseRo();
        responseRo.setStatus("branch not found");

        return new ResponseEntity(responseRo, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(AccessDeniedException.class)
    public ResponseEntity handleAccessDeniedException(AccessDeniedException ex) {
        log.warn("Access denied exception: ", ex);
        return new ResponseEntity(HttpStatus.FORBIDDEN);
    }
}

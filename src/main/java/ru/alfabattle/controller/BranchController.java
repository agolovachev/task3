package ru.alfabattle.controller;

import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.alfabattle.exception.BadRequestDataException;
import ru.alfabattle.exception.NotFoundException;
import ru.alfabattle.mapper.BranchMapper;
import ru.alfabattle.model.Branch;
import ru.alfabattle.rest.BranchRo;
import ru.alfabattle.rest.ClosestBranchResponseRo;
import ru.alfabattle.rest.PredictedLoadResponseRo;
import ru.alfabattle.service.IBranchService;

import java.math.BigDecimal;
import java.util.Optional;


@RestController
@AllArgsConstructor
@RequestMapping(path = "/branches")
public class BranchController {

    private final IBranchService branchService;
    private final BranchMapper mapper;

    @GetMapping(path = "/{id}")
    public ResponseEntity<BranchRo> getById(@PathVariable(required = false) Integer id) {

        if (id == null) {
            throw new BadRequestDataException("branch not found");
        }

        Optional<Branch> branchOpt = branchService.find(id);
        if (!branchOpt.isPresent()) {
            throw new BadRequestDataException("branch not found");
        }

        return ResponseEntity.ok(mapper.transform(branchOpt.get()));
    }

    @GetMapping
    public ResponseEntity<ClosestBranchResponseRo> getClosest(@RequestParam(required = false) BigDecimal lat,
                                                              @RequestParam(required = false) BigDecimal lon) {

        if (lat == null || lon == null) {
            throw new BadRequestDataException("branch not found");
        }
        return ResponseEntity.ok(branchService.findClosest(lat, lon));
    }

    @GetMapping(path = "/{id}/predict")
    public ResponseEntity<PredictedLoadResponseRo> predictLoad(@PathVariable(required = false) Integer id,
                                                              @RequestParam(required = false) Integer hourOfDay,
                                                              @RequestParam(required = false) Integer dayOfWeek) {

        if (id == null || hourOfDay == null || dayOfWeek == null) {
            throw new BadRequestDataException("branch not found");
        }

        if (hourOfDay < 0 || hourOfDay > 23) {
            throw new BadRequestDataException("branch not found"); //todo
        }

        if (dayOfWeek < 1 || dayOfWeek > 7) {
            throw new BadRequestDataException("branch not found"); //todo
        }

        Optional<Branch> branchOpt = branchService.find(id);
        if (!branchOpt.isPresent()) {
            throw new BadRequestDataException("branch not found");
        }

        PredictedLoadResponseRo responseRo = branchService.predict(id, hourOfDay, dayOfWeek);
        if (responseRo == null) {
            throw new NotFoundException("No data");
        }

        return ResponseEntity.ok(responseRo);
    }
}
